from .base import Token


class KeyWord(Token):
    pass


class If(KeyWord):
    token = "if"


class Else(KeyWord):
    token = "else"


class Return(KeyWord):
    token = "return"


keywords = []

for cls in list(locals().values()):
    if hasattr(cls, "mro") and Token in cls.mro() and cls is not KeyWord:
        keywords.append(cls)
