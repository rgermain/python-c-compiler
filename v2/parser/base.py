class MergeError(Exception):
    pass


class Token:
    name = None
    description = None
    token = None
    is_seprator = False
    merge_same = False

    def __init__(self, value, position):
        self.value = value
        self.position_start = position
        self.position_end = position + len(value)

    def __repr__(self):
        return f"{type(self).__name__}({self.value!r}, pos={self.position_start}:{self.position_end})"

    @classmethod
    def equal(cls, value):
        return value == cls.token

    def merge_cls(self, new, cls=None):
        if cls is None:
            cls = type(self)
        return cls(self.value + new.value, self.position_start)

    def merge_default(self, new):
        if type(new) != type(self):
            raise MergeError
        return self.merge_cls(new)

    def merge(self, new):
        raise MergeError
