from collections import deque

from .c_object import check
from .keywords import keywords
from .tokens import Digit, EndFile, Word, tokens


def merge_tokens(toks):
    new_parser = deque()
    for token in toks:
        last = new_parser[-1] if new_parser else None
        if last is None:
            new_parser.append(token)
            continue

        for m in [Word, Digit]:
            if isinstance(last, (Word, m)) and isinstance(token, m):
                last = Word(last.value + token.value, last.position_start)
                new_parser[-1] = last
                break
        else:
            new_parser.append(token)
    return new_parser


def detect_keyword(toks):
    new_parser = []

    for token in toks:
        if not isinstance(token, Word):
            new_parser.append(token)
            continue

        for keyword in keywords:
            if keyword.equal(token.value):
                new_parser.append(keyword(token.value, token.position_start))
                break
        else:
            new_parser.append(token)
    return new_parser


def is_seprator(c, position):
    for seprator in tokens:
        if seprator.equal(c):
            return seprator(c, position)


def reader(content):
    parser = deque()

    current = ""
    idx = 0
    for idx, c in enumerate(content):
        seprator = is_seprator(c, idx)
        if seprator:
            parser.append(seprator)
            current = ""
        else:
            current += c

    parser = merge_tokens(parser)
    # print(parser)
    parser.append(EndFile("", len(content)))
    parser = check(parser)
    parser = detect_keyword(parser)

    return parser
