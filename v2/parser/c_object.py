from collections import deque

from .tokens import (
    Assign,
    AttrEnd,
    Digit,
    Dot,
    DoubleDot,
    EncloseClose,
    EncloseOpen,
    EndFile,
    Hash,
    Less,
    Mult,
    NewLine,
    Slash,
    Space,
    Tab,
    Upper,
    Word,
)


class Strategy:
    def __init__(self, *tokens):
        self.tokens = tokens

    def __contains__(self, token):
        for t in self.tokens:
            if token is t:
                return True
        return False

    def equal(self, tokens, idx):
        for i, token in enumerate(self.tokens):
            if idx + i >= len(tokens):
                if len(self.tokens) != i + 1:
                    print("endd")
                    return False
                t = EndFile("", 0)
            else:
                t = tokens[idx + i]
            t_type = type(t)
            if isinstance(token, Strategy):
                if not token.equal(tokens, idx + i):
                    print("Flase, strategy")
                    return False
            elif isinstance(token, str):
                if t.value != token:
                    return False
            elif isinstance(token, CObject):
                try:
                    token.merge(tokens, idx + i)
                except MergeError:
                    return False
            elif t_type is not token:
                print("not token", t_type, token)
                return False
        return True

    def __len__(self):
        return len(self.tokens)


class StrategyOr(Strategy):
    def equal(self, tokens, idx):
        for i, token in enumerate(self.tokens):
            if idx + i >= len(tokens):
                if len(self.tokens) != i + 1:
                    return False
                t = EndFile("", 0)
            else:
                t = tokens[idx + i]
            t_type = type(t)
            if isinstance(token, Strategy):
                if token.equal(tokens, idx + i):
                    return True
            elif isinstance(token, str):
                if t.value == token:
                    return True
            elif isinstance(token, CObject):
                try:
                    token.merge(tokens, idx + i)
                except MergeError:
                    pass
                else:
                    return True

            elif t_type is token:
                return True
        return False

    def __len__(self):
        return 1


def sanitize(eq):
    if not isinstance(eq, (tuple, list)):
        eq = [eq]
    e = []
    for idx, val in enumerate(eq):
        if not isinstance(val, Strategy):
            val = Strategy(val)
        e.append(val)
    return e


def tokens_is(tokens, strategy):
    if strategy.equal(tokens, 0):
        return True
    return False


def tokens_pop(tokens, idx):
    new = []
    for _ in range(idx):
        new.append(tokens.popleft())
    return new


def token_find(tokens, strategy):
    # eq = sanitize(eq)
    for idx, _ in enumerate(tokens):
        if strategy.equal(tokens, idx):
            return idx
    return -1


class MergeError(Exception):
    pass


class CObject:
    token_end = StrategyOr(EndFile, NewLine)

    def __init__(self, value, position):
        self.value = value
        self.position_start = position
        self.position_end = position + len(value)

    def __repr__(self):
        return f"{type(self).__name__}({self.value!r}, pos={self.position_start}:{self.position_end})"

    @classmethod
    def from_token(cls, token):
        return cls(token.value, token.position_start)

    @classmethod
    def merge(cls, tokens):
        print(tokens_is(tokens, cls.token_start))
        if not tokens_is(tokens, cls.token_start):
            raise MergeError
        # print(tokens)
        print(token_find(tokens, cls.token_end))
        idx = token_find(tokens, cls.token_end)
        if idx == -1:
            raise MergeError

        values = tokens_pop(tokens, idx + len(cls.token_end))
        value = "".join([v.value for v in values])
        return cls(value, values[0].position_start)


# class SingleComment(CObject):
#     token_start = Strategy(Slash, Slash)


# class MultipleComment(CObject):
#     token_start = Strategy(Slash, Mult)
#     token_end = Strategy(Mult, Slash)


# class PreprocessInclude(CObject):
#     token_start = Strategy(
#         Hash, "include", StrategyOr(Space, Tab), Less, Word, Dot, Word, Upper
#     )


class Exp(CObject):
    token_start = Strategy(
        Word,
        StrategyOr(Space, Tab),
        Word,
        StrategyOr(Space, Tab),
        Assign,
        # StrategyOr(Space, Tab),
        Digit,
    )
    token_end = Strategy(AttrEnd)


# class Function(CObject):
#     token_start = Strategy(
#         "def",
#         StrategyOr(Space, Tab),
#         Word,
#         EncloseOpen,
#         EncloseClose,
#         DoubleDot,
#         NewLine,
#         StrategyOr(Strategy(Exp), SingleComment, MultipleComment),
#         NewLine,
#         NewLine,
#     )


c_object = []

for cls in list(locals().values()):
    if hasattr(cls, "mro") and CObject in cls.mro() and cls is not CObject:
        c_object.append(cls)


class Unknow(CObject):
    pass


def check(tokens):
    new = deque()
    while tokens:
        for obj in c_object:
            try:
                new.append(obj.merge(tokens))
            except MergeError:
                pass
            else:
                break
        else:
            new.append(Unknow.from_token(tokens.popleft()))

    if isinstance(new[-1], EndFile):
        new.pop()
    return new
