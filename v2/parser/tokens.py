from .base import Token


class Adress(Token):
    token = "&"


class Tab(Token):
    token = "\t"


class Space(Token):
    token = " "


class NewLine(Token):
    token = "\n"


class EncloseOpen(Token):
    token = "("


class EncloseClose(Token):
    token = ")"


class Hash(Token):
    token = "#"


class ScopeOpen(Token):
    token = "{"


class ScopeClose(Token):
    token = "}"


class AttrEnd(Token):
    token = ";"


class SingleQuote(Token):
    token = "'"


class DoubleQuote(Token):
    token = '"'


class AntiSlash(Token):
    token = "\\"


class Slash(Token):
    token = "/"


class Operator(Token):
    pass


class Assign(Operator):
    token = "="


class Add(Operator):
    token = "+"


class Neg(Operator):
    token = "-"


class Div(Token):
    token = "/"


class Mult(Operator):
    token = "*"


class Less(Operator):
    token = "<"


class Upper(Operator):
    token = ">"


class Not(Operator):
    token = "!"


class Dot(Operator):
    token = "."


class DoubleDot(Operator):
    token = ":"


class Digit(Token):
    @classmethod
    def equal(cls, value):
        return value.isdigit()


class Word(Token):
    @classmethod
    def equal(cls, value):
        if not value[0].isalpha() and value[0] not in ["_"]:
            return False
        return True


tokens = []

for cls in list(locals().values()):
    if hasattr(cls, "mro") and Token in cls.mro() and cls is not Token:
        tokens.append(cls)


class Unknow(Token):
    pass


class EndFile(Token):
    token = ""
