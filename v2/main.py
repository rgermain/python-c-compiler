from parser.reader import reader


def main():
    with open("./main.c") as f:
        content = f.read()

    tokens = reader(content)
    for t in tokens:
        print(t)


if __name__ == "__main__":
    main()
