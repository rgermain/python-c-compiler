import threading
import time

lock = threading.RLock()
# lock = threading.Semaphore(3)

e = threading.Event()


def d(i):
    for _ in range(10000):
        print(f"salut {i} {threading.get_ident()}")
        time.sleep(0.1)


ts = [threading.Thread(target=d, args=(0,)), threading.Thread(target=d, args=(1,))]

s = time.perf_counter()
[t.start() for t in ts]
[t.join() for t in ts]
e = time.perf_counter()

print(e - s)
