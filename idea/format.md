## Format string

```c
int main() {

    char *name = "Remi";
    int age = 30;

    printf(f"{name=} {age=}")
    printf(f"{name=} {age:.0}")
    printf(f"{name!p} {age:32.0!lld}")
}
```
> output
```c
int main() {

    char *name = "Remi";
    int age = 30;

    printf("name=%s age=%d", name, age)
    printf("name=%s %.0d", name, age)
    printf("%p %32.0lld", name, age)
}
```