## Assert
```c
int main() {
    int a = 0;
    assert (a != 0), "invalid value";
}
```
> output
```c
void internal__assert(bool value, const char *msg, char *file, uint32_t lines) {
    if (value) {
        return;
    }
    panic(1, "Assertion error: %s\nFile: %s:%d\n", msg, file, lines)
}

int main() {
    int a = 0;
    #if __ASSERT__
        internal__assert((a != 0), "invalid value", "./main.c", 15);
    #endif
}
```