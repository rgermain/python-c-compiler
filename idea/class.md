# class
```c
class A {
    public int a;
    private char *name;
    void __init__() {
        this.name = malloc(sizeof(char) * 8);
        strcopy(this.name, "coucou");
    }

    void __delete__() {
        free(this.name)
    }

}

class B(A) {
    public int age = 0;
    void __init__() {
        this.parent.__init__()
    }

    char *get_name() {
        return this.name
    }

    void set_name(char *value) {
        free(this.name)
        this.name = value;
    }
}

int main() {
    class B cls = new B();

    printf("{cls.get_name()=}")
    cls.set_name("yep")
    printf("{cls.get_name()=}")
}
```

```c
/* class A */
struct internal__class__A {
    int a;
    char * name;
};

void internal__class__A___init__(struct internal__class__A * this) {
    this->name = malloc(sizeof(char) * 8);
    strcpy(this->name, "coucou");
}

void internal__class__A___delete__(struct internal__class__A * this) {
    free(this->name);
}

/* class B */
struct internal__class__B {
    struct internal__class__A parent;
    int age;
};

void internal__class__B___init__(struct internal__class__B * this) {
    this->age = 0;
    internal__class__A___init__(&this->parent);
}

char *internal__class__B_get_name(struct internal__class__B * this) {
    return this->parent.name;
}

char *internal__class__B_set_name(struct internal__class__B * this, const char * value) {
    free(this->parent.name);
    this->parent.name = malloc(sizeof(char*) * 10);
    strcpy(this->parent.name, value);
}


/* main */
int main() {
    struct internal__class__B cls;
    internal__class__B___init__(&cls);

    printf("name=%s", internal__class__B_get_name(&cls));
    internal__class__B_set_name(&cls, "yep");
    printf("name=%s", internal__class__B_get_name(&cls));

}
```