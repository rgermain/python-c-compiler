
## Label
```c
int main() {
    ...
    if (value) {
        printf("...")
    } else if (value2) {
        goto main:before_exit;
    } else {
        goto exit;
    }

    int result = func(...)

    label before_exit;
    reset();
    ...

    label exit;
    
    return 0;
}
```
> output
```c
int main() {
    ...
    if (value) {
        printf("...")
    } else if (value2) {
        goto internal_label__main__before_exit:
    } else {
        goto internal_label__main__exit:
    }

    int result = func(...)

    internal_label__main__before_exit:
    reset();
    ...

    internal_label__main__exit:
    
    return 0;
}
```
