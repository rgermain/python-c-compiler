
## Type byte
```c
int main() {
    byte u = 0;
    u = 1;

    byte u[100];
    u[1] = value;
}
```
> output
```c

#define INDEX_BYTES(_size, _type) (_size / sizeof(_type))

#define POSITION_BYTES(_size, _type) (_size % sizeof(_type))

#define SIZE_BYTES(_size, _type) (INDEX_BYTES(_size, _type) + (POSITION_BYTES(_size, _type) ? 1 : 0))


int main() {
    uint8 u = 0;
    u = 1;

    uint8 u[SIZE_BYTES(100, uint8)];
    u[INDEX_BYTES(1, uint8)] = (value ? 1 : 0) << POSITION_BYTES(1, uint8);
}
```