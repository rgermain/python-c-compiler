

## Debug structure
```c
struct Person {
    char *name;
    int age;
    bool alive;
}

int main() {
    struct Person remi = {
        .name = "remi",
        .age = 30,
        .alive = true,
    };
    printf(f"{remi!debug}")
}
```
>output
```c
struct Person {
    char *name;
    int age;
    bool alive;
};

int main() {
    struct Person remi = {
        .name = "remi",
        age = 30,
        alive = true,
    };
    printf(f"<Struct Person(%p), name=%s, age=%d, alive=%s>", remi, remi.name, remi.age, remi.alive ? "true" : "false");
}
```