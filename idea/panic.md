## panic
```c
int main(int ac, char **av) {
    if (ac >= 10) {
        panic("ar counter need to be less than 10")
    }
    return 0
}
```
> output
```c
void panic(uint8_t exit_code, const char *msg, ...) {
    dprintf(2, msg, ...);
    exit(exit_code);
}
```