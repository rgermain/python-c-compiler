## Return multiple value
```c
(int, int) divmod(int a, int b) {
    return (
        a / b,
        a % b
    );
}

int main() {
    int (div, mod) = divmod(100, 5);
}
```
> output
```c
struct internal__divmod {
    int value1;
    int value2;
};

struct internal__divmod divmod(int a, int b) {
    struct internal__divmod __results_pack;

    __results_pack.value1 = (a / b);
    __results_pack.value2 = (a % b);

    return __results_pack;
}

int main() {

    int div;
    int mod;

    {
        struct internal__divmod __results_divmod = divmod(100, 5);
        div = __results_divmod.value1;
        mod = __results_divmod.value2;
    }
}
```
