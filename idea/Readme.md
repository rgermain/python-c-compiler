



## loop infite ???
```c
int main() {
    unlikely {

    }
}
```

## Error ???
```c
int main() {
    with open("file.txt", O_RDONLY | O_NOCTTY) as fd {
        char buffer[50];
        read(fd, value, 50)
    } error {
        exit(0);
    }
}
```
>output
```c
int main() {
    int fd = open("file.txt", O_RDONLY | O_NOCTTY);
    if (fd == -1) {
        exit(0);
    }

    char buffer[50];
    read(fd, value, 50)

    close(fd);
}
```
