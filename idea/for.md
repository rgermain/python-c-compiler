## forloop

```c
int main(int ac, char **av) {
    for (auto el in av) {
        print(f"{el}");
    }

    macro for (auto (idx, el) in enumerate(av)) {
        print(f"{el}");
    }

}
```
> output
```c

struct internal__enumerate {
    int32_t index;
    void *value;
}

(int32_t, void*) enumerate(void* iterator) {

}

int main(int ac, char **av) {
    {
        int i = 0;
        while (av[i]) {
            char *el = av[i];

            printf("%s", el);

            i++;
        }
    }
}
```

