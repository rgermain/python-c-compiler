## alloc
```c
from ctop.string import strnnew, literal

int main(int ac, char **av) {
    char *name = new "salut"

    char *name = strnnew(av[1], 4, 10);

    unsafe char *name = malloc(sizeof(...));
}
```
> output
```c
char *internal_new(const char *src) {
    char *new = malloc(sizeof(char) * (strlen(src) + 1));
    if (!new) {
        panic("Can't allocate memory");
    }
    strcpy(src, new);
    return new;
}
int main(int ac, char **av) {
    char *name = 
}
```