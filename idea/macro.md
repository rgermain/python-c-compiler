## Macro

```c

define macro "for-iterator" {
    syntax = "for" "(" exp:assing as left "in" [exp:var, exp:function] as right ")" exp:scope as scope
    result = {
        
        if (right.is_function && right.result == struct:iterator) {
            return """
            {
                auto ${left} = ${right};
                while (${left[1].name}[${left[0].name}] != NULL) {
                    ${scope}
                    ${left[0].name} += 1;
                }
            }
            """
        }
    }
}

define macro "assert" {
    syntax = "assert" exp:condition macro:optional("," exp:var(char*))
}

define macro "f-string" {
    syntax = "f" + exp:string
    result = {
        return 
    }
}
```