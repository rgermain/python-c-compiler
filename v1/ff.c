#include <stdlib.h>
#include <string.h>

int function() {
    return 0;
}

int main() {
    char *s = malloc(6);
    strcpy(s, "salut");
    s[5] = 0;
    s[1] = 'e';

    int ret = function();
    if (ret == 0) {
        free(s);
    } else if (ret == 9) {
        free(s);
    }
    return 0;
}