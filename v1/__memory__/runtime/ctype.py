
class CType:
    @property
    def skeleton(self):
        return self.token
    
    @property
    def prototype(self):
        return self.token

class Char(CType):
    token = "char"
    bytes = 8

class Int8(CType):
    token = "int"
    bytes = 8

class Int16(CType):
    token = "int"
    bytes = 16

class Int32(CType):
    token = "int"
    bytes = 32

class Int64(CType):
    token = "int"
    bytes = 64

class Int128(CType):
    token = "int"
    bytes = 128

class Short(CType):
    token = "short"
    bytes = 16

class Pointer(CType):
    token = "*"

    def __init__(self, to):
        self.to = to

    @property
    def skeleton(self):
        return f"{self.to.skeleton}{self.token}"

class Array(Pointer):
    token = "[]"

    def __init__(self, to, size):
        super().__init__(to)
        self.size = size

    @property
    def skeleton(self):
        return f"{self.to.skeleton}[{self.size}]"
