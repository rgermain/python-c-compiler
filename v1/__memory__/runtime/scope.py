class Scope:
    have_scope = True
    def __init__(self, instructions, indentation=0):
        self.instructions = instructions
        self.indentation = indentation
    
    def indente(self, value, indentation=None):
        if indentation is None:
            indentation = self.indentation
        prefix = "\t" * indentation
        return f"{prefix}{value}"

    def conv_instructions(self):
        join = ""
        for inst in self.instructions:
            if isinstance(inst, self.__class__):
                join += inst.to_c(self.indentation)
            elif inst.have_scope:
                join += self.indente(f"{inst.to_c()}\n")
                join += inst.scope.to_c(self.indentation)
            else:
                join += self.indente(f"{inst.to_c()}") + ";"
            join += "\n"
        return join

    def to_c(self, indentation=0):
        prefix = self.indente("", indentation)
        self.indentation = indentation + 1
        return prefix + "{\n" + self.conv_instructions() + prefix + "}"


class FileScope(Scope):
    def __init__(self, file_name, instructions):
        self.file_name = file_name
        super().__init__(instructions)

    def to_c(self):
        return self.conv_instructions()