def isparent_instance(src, parent):
    return parent in src.__class__.mro()