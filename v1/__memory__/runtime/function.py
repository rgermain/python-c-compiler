from .runtime import RunTimeC

class Function(RunTimeC):
    have_scope = True

    def __init__(self, name, arguments, return_type, scope, **kwargs):
        super().__init__(**kwargs)
    
        self.name = name
        self.arguments = arguments
        self.return_type = return_type
        self.scope = scope

        assert isinstance(self.arguments, Arguments), f"arguments need to be `{Arguments.__name__}` instance, not {type(self.arguments)}"


    def to_c(self):
        return self.prototype

    @property
    def skeleton(self):
        return f"({self.return_type.skeleton})(*fnc)({self.arguments.skeleton})"
    
    @property
    def prototype(self):
        return f"{self.return_type.prototype} {self.name}({self.arguments.prototype})"


class Arguments(RunTimeC):

    def __init__(self, *arguments, **kwargs):
        super().__init__(**kwargs)
        
        self.arguments = arguments

    def to_c(self):
        return ", ".join([variable.prototype for variable in self.arguments])
    
    @property
    def skeleton(self):
        return ", ".join([variable.skeleton for variable in self.arguments])
    
    @property
    def prototype(self):
        return ", ".join([variable.prototype for variable in self.arguments])