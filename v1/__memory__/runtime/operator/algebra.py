from .operator import Operator, SelfAssign

class Add(Operator):
    token = "+"

class Sus(Operator):
    token = "-"

class Div(Operator):
    token = "/"

class Mult(Operator):
    token = "*"

class Mod(Operator):
    token = "%"

class AssignAdd(SelfAssign, Add):
    pass
class AssignSus(SelfAssign, Sus):
    pass
class AssignDiv(SelfAssign, Div):
    pass
class AssignMult(SelfAssign, Mult):
    pass
class AssignMod(SelfAssign, Mod):
    pass