from ..runtime import RunTimeC

class Operator(RunTimeC):
    def __init__(self, src, dest, **kwargs):
        super().__init__(**kwargs)
        self.src = src
        self.dest = dest
    
    def to_c(self):
        return f"({self.src.to_c()} {self.token} {self.dest.to_c()})"

class SelfAssign(Operator):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.token += "="

    def to_c(self):
        return f"{self.src.to_c()} {self.token} {self.dest.to_c()}"