from .operator import Operator
from .operator import SelfAssign

class And(Operator):
    token = "&&"

class Or(Operator):
    token = "||"


class ShiftLeft(Operator):
    token = "<<"

class ShiftRight(Operator):
    token = ">>"

class Or(Operator):
    token = "|"

class Xor(Operator):
    token = "^"

class And(Operator):
    token = "&"


class AssignShiftLeft(SelfAssign, ShiftLeft):
    pass
class AssignShiftLeft(SelfAssign, ShiftLeft):
    pass
class AssignShiftRight(SelfAssign, ShiftRight):
    pass
class AssignOr(SelfAssign, Or):
    pass
class AssignXor(SelfAssign, Xor):
    pass
class AssignAnd(SelfAssign, And):
    pass