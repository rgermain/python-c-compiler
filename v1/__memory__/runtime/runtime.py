class RunTimeC:
    have_scope = False

    def to_str(self):
        raise Exception("you need to implement this function")

    @property
    def skeleton(self):
        raise Exception("you need to implement this function")
    
    @property
    def skeleton(self):
        raise Exception("you need to implement this function")

    def conv_value(self, value):
        if hasattr(value, "to_c"):
            return value.to_c()
        if isinstance(value, str):
            return f'"{value}"'
        return str(value)