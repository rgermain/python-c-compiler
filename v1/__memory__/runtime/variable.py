from .runtime import RunTimeC
from .ctype import CType
from .utils import isparent_instance

class Variable(RunTimeC):

    def __init__(self, name, typedef, value=None, const=False, static=False, unsigned=False, **kwargs):
        super().__init__(**kwargs)

        self.name = name
        self.typedef = typedef
        self.const = const
        self.static = static
        self.unsigned = unsigned
        self.value = value
        assert isparent_instance(typedef, CType), f"instance of type need to be a CType not `{type(self.typedef)}`"

    def to_c(self):
        if self.value is None:
            return self.skeleton
        return f"{self.skeleton} = {self.conv_value(self.value)}"
        

    @property
    def skeleton(self):
        s = ""
        if self.static:
            s += "static "
        if self.const:
            s += "const "
        if self.unsigned:
            s += "unsigned "
        return f"{s}{self.typedef.skeleton} {self.name}"
    
    @property
    def prototype(self):
        return self.skeleton

# shortcut
Vars = Variable

class StackVariable(Variable):
    def __init__(self, typedef, value, **kwargs):
        super().__init__(None, typedef, value, **kwargs)

    def __str__(self):
        return f"STACK {str(super())}"

    def to_c(self):
        return self.value

    @property
    def skeleton(self):
        raise Exception("no skeleton available for stack variable")


class HeapVariable(Variable):
    def __init__(self, name, typedef, value, size, **kwargs):
        super().__init__(name, typedef, value, **kwargs)
        self.size = size

    def __str__(self):
        return f"HEAP {str(super())}"

class VarRef(Variable):
    """ variable with reference other variable"""
    def __init__(self, name, **kwargs):
        super(RunTimeC, self).__init__(**kwargs)
        
        self.name = name
        self.typedef = None
        self.const = False
        self.static = False
        self.unsigned = False
    
    def to_c(self):
        return self.name