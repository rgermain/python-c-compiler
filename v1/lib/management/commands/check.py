from .base import BaseCommand

class CheckCommand(BaseCommand):
    name = "check"
    description = "check src file"

    def add_argument(self, parser):
        super().add_argument(parser)
        parser.add_argument("--entry-file", action="store", help="check entry file", default=None)
        parser.add_argument("-r", "--recursive", action="store_false", help="follow all include file", default=True)

    def execute(self, opt):
        recursive = opt.recursive
        check_file = opt.entry_file or self.config['builder']['entry']

        print(f"we check file {check_file} in rcurisve: {recursive}")