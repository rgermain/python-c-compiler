import argparse
from ...config import get_config

class BaseCommand:
    name = None
    description = None
    description_long = None

    def __init__(self, name_prog, args):

        assert isinstance(self.name, str), "You need to set name"
        assert isinstance(self.description, str), "You need to set description"
    
        self.__args = args

        self.__argparse = argparse.ArgumentParser(description=self.description, prog=name_prog)
        self.__argparse.add_argument("-v", "--verbose", action="store_true", default=False)
        self.__argparse.add_argument("-d", "--root-dir", action="store", default="project")
        self.__argparse.add_argument("-f", "--config-file", action="store", default="c.config.yaml")
        self.__argparse.add_argument('args', nargs='*')
    
    def add_argument(self, parser):
        pass

    def run(self):
        self.add_argument(self.__argparse)
        opt = self.__argparse.parse_args(args=self.__args)
        self.config = get_config(opt.config_file)
        self.execute(opt)
    
    def execute(self, opt):
        raise Exception("You need to ovelap this method")