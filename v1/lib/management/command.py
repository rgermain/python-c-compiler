from .execption import CommandNotFound
import glob
import importlib
import os
import sys
from .commands.check import CheckCommand
from lib.utils.core import panic 

def get_all_commands():
    commands = [
        CheckCommand,
    ]
    return commands

def serialize(all_command):
    cmds = {}
    for cmd in all_command:
        if cmd.name in cmds:
            panic(f"duplicate command `{cmd.name}` from `{cmd['path']}`")
        cmds[cmd.name] = cmd
    
    return cmds

def get_command(name_cmd):
    """get class of command name"""

    all_command = get_all_commands()
    all_command = serialize(all_command)
    try:
        return all_command[name_cmd]
    except (IndexError, KeyError) as e:
        raise CommandNotFound(f"command {name_cmd} is not found")
    
