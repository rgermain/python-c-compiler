from lib.utils.dict import merge
from lib.utils.core import panic
from lib.utils.path import relative
import yaml

def read_config(config_file):
    """open config file"""
    try:
        with open(config_file, "r") as f:
            try:
                value = f.read()
                config = yaml.safe_load(value)
            except Exception as e:
                panic("Invalid `yaml` file!", e)
    except Exception as e:
        panic(f"c'ant open file `{config_file}`", e)
    return config

def merge_config(config):
    """merge the config with default config"""
    default = read_config(f"{relative(__file__)}/default_config.yaml")
    if default is not None:
        return merge(default, config)
    return config

def check_config(config):
    """ check the config value """
    pass

def get_config(config_file):
    """ get the config file """
    config = read_config(config_file)
    if config is None:
        panic("the config file is not found !")
    return merge_config(config)