class Storage:
    @property
    def declaration(self):
        return f"{self.token} {self.variable.declaration}"

class Auto(Storage):
    token = "auto"

    def __init__(self, *args, show=False, **kwargs):
        self.show = show
        super().__init__(*args, **kwargs)

    @property
    def declaration(self):
        if self.show:
            return super().declaration
        return self.variable.declaration

class Static(Storage):
    token = "static"

class Extern(Storage):
    token = "extern"

class Register(Storage):
    token = "register"
