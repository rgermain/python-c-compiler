from .base import Operator

class Assign(Operator):
    token = "="


class Increment(Assign):
    token = "++"

    def __init__(self, data1):
        super().__init__(self, data1, None)
    
    @property
    def to_c(self):
        return f"{self.data1}{self.token}"

class Decrement(Assign):
    token = "--"

    def __init__(self, data1):
        super().__init__(self, data1, None)
    
    @property
    def to_c(self):
        return f"{self.data1}{self.token}"