from .base import Operator

class ShiftLeft(Operator):
    token = "<<"

class ShiftRight(Operator):
    token = ">>"

class Ones(Operator):
    token = "~"

class And(Operator):
    token = "&"

class And(Operator):
    token = "&"

class Or(Operator):
    token = "|"

class Xor(Operator):
    token = "^"