class Operator:

    def __init__(self, data1, data2):
        self.data1 = data1
        self.data2 = data2

    @property
    def instruction():
        return f"{self.data1} {self.token} {self.data2}"