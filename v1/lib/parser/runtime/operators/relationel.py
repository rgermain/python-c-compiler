from .base import Operator

class Equal(Operator):
    token = "=="

class NotEqual(Operator):
    token = "!="

class Less(Operator):
    token = "<"

class Greater(Operator):
    token = ">"

class LessEuqal(Operator):
    token = "<="

class GreaterEqual(Operator):
    token = ">="
