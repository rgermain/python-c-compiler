from .base import Operator

class Sizeof(Operator):
    token = "sizeof"

    def __init__(self, data1):
        super().__init__(self, data1, None)

    @property
    def to_c(self):
        return f"{self.token}({self.data1.instruction})"

class Adress(Operator):
    token = "&"

    def __init__(self, data1):
        super().__init__(self, data1, None)

    @property
    def to_c(self):
        return f"{self.token}{self.data1.instruction}"

class Pointer(Operator):
    token = "*"

    def __init__(self, data1):
        super().__init__(self, data1, None)

    @property
    def instruction(self):
        return f"{self.token}{self.data1.instruction}"