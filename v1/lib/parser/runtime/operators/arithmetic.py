from .base import Operator

class Addition(Operator):
    token = "+"

class Subtraction(Operator):
    token = "-"

class Multiplication(Operator):
    token = "*"

class Division(Operator):
    token = "/"

class Modulus(Operator):
    token = "%"
