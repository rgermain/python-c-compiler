from .base import Operator

class And(Operator):
    token = "&&"

class Or(Operator):
    token = "||"

class Not(Operator):
    token = "!"