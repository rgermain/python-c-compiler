import sys
from .colors import red

def panic(info, err=None):
    print(f"{red('Error')}: {info}\nexit...", file=sys.stderr)
    exit(1)