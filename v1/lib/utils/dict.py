
def merge(src, des):
    """
        deep merge 2 dictionary
    """
    for key, value in src.items():
        if isinstance(value, dict):
            node = des.setdefault(key, {})
            merge(value, node)
        else:
            des[key] = value
    return des