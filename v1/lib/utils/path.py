import os

def relative(_from, to=""):
    return os.path.dirname(_from) + to
