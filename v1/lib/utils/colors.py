import colorama
colorama.init()

def red(st):
    return f"{colorama.Fore.RED}{st}{colorama.Style.RESET_ALL}"