import argparse
from .management.execption import CommandNotFound
from .management.command import get_command

def execute_from_cli(args):
    prog_name = args.pop(0)
    try:
        command_name = args.pop(0)
    except IndexError:
        command_name = "usage"
    
    try:
        cl = get_command(command_name)
        cl(prog_name, args).run()
    except CommandNotFound:
        panic(f"command `{command_name}` not found")
    