from __memory__.runtime.scope import Scope, FileScope
from __memory__.runtime.function import Function, Arguments
from __memory__.runtime.ctype import Char, Int32, Pointer
from __memory__.runtime.variable import Variable, StackVariable, VarRef, Vars
from __memory__.runtime.operator.algebra import AssignAdd, Mult, Div, Sus, AssignSus, AssignMod
from __memory__.runtime.operator.binary import ShiftLeft, Xor, AssignOr

class Cast:
    pass


def main():
    file_scope = FileScope("main.c", [
        Vars("i", Int32(), 43, const=False),
        Vars("s", Int32(), 42),
        Vars("cw", Int32(), 1),
        Function("main", Arguments(Variable("argc", Int32()), Variable("argv", Pointer(Pointer(Char())))), Int32(), Scope([
            AssignAdd(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
            AssignOr(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
            AssignAdd(VarRef("i"), Mult(Sus(VarRef("cw"), StackVariable(Int32(), 4)), Sus(StackVariable(Int32(), 777), VarRef("i")))),
            AssignAdd(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
            Scope([
                AssignAdd(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
                AssignAdd(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
                AssignAdd(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
            ]),
            AssignAdd(VarRef("i"), Mult(ShiftLeft(VarRef("cw"), StackVariable(Int32(), 4)), Xor(StackVariable(Int32(), 777), VarRef("i")))),
        ]))
    ])
    with open("result.c", "w") as f:
        f.write(file_scope.to_c())
    print(f"|{file_scope.file_name}|\n>>\n{file_scope.to_c()}<<")

if __name__ == "__main__":
    main()
