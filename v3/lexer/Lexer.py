from .tokens import DECLARED_TOKENS, Unknow


class Lexer:
    def __init__(self, file):
        self.file = file
        with open(file) as f:
            self.content = f.read()

    def tokenizers(self):
        tokens = self.generate_tokens()
        tokens = self.merge_tokens(tokens)

        return tokens

    def generate_tokens(self):
        tokens = []
        for position, c in enumerate(self.content):
            for token in DECLARED_TOKENS:
                if token.equal(c):
                    tokens.append(token(c, position))
                    break
            else:
                tokens.append(Unknow(c, position))
        return tokens

    def merge_tokens(self, tokens):
        new_tokens = []
        for token in tokens:
            if not new_tokens:
                new_tokens.append(token)
                last = new_tokens
                continue

            last = new_tokens[-1]

            if last.ismergable(token):
                last.merge(token)
            else:
                new_tokens.append(token)

        return new_tokens
