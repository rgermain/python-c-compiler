class Token:
    mergable = False

    def __init__(self, content, position):
        self.content = content
        self.position = position

    def __repr__(self):
        return f"<{type(self).__name__} content={self.content!r} position={self.position} />"

    @classmethod
    def equal(cls, c):
        return cls.token == c

    def ismergable(self, token):
        return self.mergable

    def merge(self, token):
        self.content += token.content


class Number(Token):
    @classmethod
    def equal(cls, c):
        return c.isdigit()

    def ismergable(self, token):
        return isinstance(token, type(self))


class Plus(Token):
    token = "+"


class Sub(Token):
    token = "-"


class Div(Token):
    token = "/"


class Mult(Token):
    token = "*"


class Mod(Token):
    token = "%"


class Preprocessor(Token):
    token = "#"


class EndInst(Token):
    token = ";"


class Space(Token):
    @classmethod
    def equal(cls, c: str):
        return c in [" ", "\t"]

    def ismergable(self, token):
        return isinstance(token, type(self))


class NewLine(Token):
    @classmethod
    def equal(cls, c: str):
        return c == "\n"

    def ismergable(self, token):
        return isinstance(token, type(self))


class Equal(Token):
    token = "="


class LeftBracket(Token):
    token = "{"


class RightBracket(Token):
    token = "}"


class LeftPair(Token):
    token = "("


class RightPair(Token):
    token = ")"


class LeftCracket(Token):
    token = "["


class RightCracket(Token):
    token = "]"


class Dot(Token):
    token = "."


class Comma(Token):
    token = ","


class SingleQuote(Token):
    token = "'"


class DoubleQuote(Token):
    token = '"'


class BackSlash(Token):
    token = "\\"


class And(Token):
    token = "&"


class Or(Token):
    token = "|"


class Not(Token):
    token = "!"


class Xor(Token):
    token = "^"


class LeftShift(Token):
    token = "<"


class RightShift(Token):
    token = ">"


class Word(Token):
    @classmethod
    def equal(cls, c: str):
        return c.isalpha() or c in ["_"]

    def ismergable(self, token):
        return isinstance(token, type(self))


DECLARED_TOKENS = []
lvars = dict(vars())

for el in lvars.values():
    if hasattr(el, "mro") and Token in el.mro() and el != Token:
        DECLARED_TOKENS.append(el)


class Unknow(Token):
    token = None
