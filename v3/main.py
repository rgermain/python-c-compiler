import argparse

from lexer import Lexer


def main(flags):
    lex = Lexer(flags.file)
    tokens = lex.tokenizers()
    print(tokens)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    flags = parser.parse_args()

    main(flags)
